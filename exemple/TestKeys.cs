﻿namespace exemple
{
    static public class TestKeys
    {
        public static readonly string PublicKey = @"
-----BEGIN PUBLIC KEY-----
MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCg5Jy7P7ai/6VvjUYT3yFY3DbZ
f5yYE2DdZWNVJzwW+VDQNdtM5SZ4wVjSqL1qnqW2oicFTTafZ4rTn4ecN8y/Nmmn
D3aLM2wVu/rU6sB3n3M+UlXCQt3PlSSuOwKBw5+l7UkxvGGDiGxoEBJAmBto/XEJ
KNZpK2uSVxLBWoNSwwIDAQAB
-----END PUBLIC KEY-----";
        public static readonly string PrivateKey = @"
-----BEGIN RSA PRIVATE KEY-----
MIICXgIBAAKBgQDWhb8WXYioyiN+zb5K/gwlXzf1DuW9z+c3wDU0SNjODuRhpUi0
EotPyZ6z9vhdniwDgz/Y+nSV1G094BrKUAW8ls3PGKnn0wFn7AF9HdMsEBR0xxdv
JXNvsgOqbobMfLwKZkaEGyOYoNoYDS3NVS2xriipmd5Rj/+AR0NybOzCbwIDAQAB
AoGBALZBFRKsTpGQgs2VgMl3wtvxZLoXeQplWryb2Nfd2AxgzJlE8lrCsocL9YBS
H7pxylUFqjXuuyQcQN9jyX7+s49yF1fk2oMr38CC1beK5eAegqk+7bAusyx6pQ64
R8CKARJKISBxcQVN8aj9m5/IPhv4ir9Jw33nZ/8VhyOQsjRhAkEA9t1DwSfvnlY9
9Ecnk7FpzP3dpBf7ENNT3DDichXDncrGkRd4+pT6VsbHwG2tW52B/VCq+Faod5o+
L3A/IPSNnwJBAN52FRhZxWFLiGmkOewmbO4V3qEpTnplTo3iTArGd4bfqc5aqPY9
101SVyPcDM/ZGNKxpAZS1DRfnnlTF4bi+TECQEJh13FeYtHqEPjaFWtivreP4ITa
qfmpShig0JOrBJIuz9x+Olrvqq2hAF/fXkmPfj/27DKhYuXehTBOd7tN57MCQQCc
q5bBtn9evT5OAB3W5Vzz9Cz9XUTqyCwhmo0pTYriTuSc8pZqDTVzPBkopRHU07U6
tWaqGW1fkTPfiBhao8ERAkEAwlWyFiMZaggyRXeG6R4A8tTxu2z/gKZDI9nUV0It
uCCNuUH+mFwP2CZWL5zqkAyLnqXdAArMJu2kryXs9I2nuw==
-----END RSA PRIVATE KEY-----";
    }
}
