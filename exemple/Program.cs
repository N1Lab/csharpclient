using System;
using System.IO;
using N1;
using N1.Exceptions;
using N1.Utilities;
using N1.Xml;

namespace exemple
{
	class MainClass
	{
		public static void Main (string[] args)
		{
		    var consoleWindowWidth = Console.LargestWindowWidth*0.8;
            Console.SetWindowSize((int)consoleWindowWidth, Console.LargestWindowHeight / 2);
            Console.Title = "N1 client exemple";
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Welcome to N1 client exemple");
            Console.ResetColor();
			var config = new Config
			{
				MerchantName = "34f34g34g", 
				ServiceName = "wefwef",
				RequestTimeout = 12000,
				PrivateKey = TestKeys.PrivateKey,
				PublicKey = TestKeys.PublicKey
			};
            Console.WriteLine("Enter [create,confirm,status,notify]");
            var type = Console.ReadLine();
		    if (type == "create")
		    {
                Create(config);
		    }
            else if (type == "confirm")
		    {
		        Confirm(config);
		    }
            else if (type == "status")
            {
                Status(config);
            }
            else if (type == "notify")
            {
                Notify(config);
            }
		    else
		    {
                Console.WriteLine("Wrong '{0}' type!", type);
		    }
            Console.ReadKey();
		}

	    private static void Create(Config config)
	    {
            var client = new OrderClientMerchant(config);
            var mobile = client.GetMobilePayment();
            Console.Write("Enter the amount[10]: ");
            var amount = Console.ReadLine();
            if (String.IsNullOrEmpty(amount))
            {
                amount = "10";
            }
            Console.Write("Enter the account[listepo]: ");
            var account = Console.ReadLine();
            if (String.IsNullOrEmpty(account))
            {
                account = "listepo";
            }
            Console.Write("Enter the service[wefwef]: ");
            var service = Console.ReadLine();
            if (String.IsNullOrEmpty(service))
            {
                service = "wefwef";
            }
            Console.Write("Enter the phone number[380674981206]: ");
            var phoneNumber = Console.ReadLine();
            if (String.IsNullOrEmpty(phoneNumber))
            {
                phoneNumber = "380674981206";
            }
            try
            {
                var response = mobile.Create(
                    Guid.NewGuid().ToString("N"),
                    Decimal.Parse(amount),
                    account,
                    service,
                    phoneNumber
                );
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Status: Success");
                Console.ResetColor();
            }
            catch (ApiException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Status: Fail. Error code: {0}, message: {1}", e.ErrorCode, e.Message);
                Console.ResetColor();
            }
            Console.WriteLine("Request xml: ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(mobile.GetRequestXml());
            Console.ResetColor();
            Console.WriteLine("Response xml: ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(mobile.GetResponseXml());
            Console.ResetColor();
	    }

	    private static void Confirm(Config config)
        {
            var client = new OrderClientMerchant(config);
            var mobile = client.GetMobilePayment();
            Console.Write("Enter the transaction id: ");
            var transactionId = Console.ReadLine();
            if (String.IsNullOrEmpty(transactionId))
            {
                transactionId = "";
            }
            Console.Write("Enter the confirm code: ");
            var code = Console.ReadLine();
            try
            {
                var response = mobile.Confirm(UInt64.Parse(transactionId), code);
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Status: Success");
                Console.ResetColor();
            }
            catch (ApiException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Status: Fail. Error code: {0}, message: {1}", e.ErrorCode, e.Message);
                Console.ResetColor();
            }
            Console.WriteLine("Request xml: ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(mobile.GetRequestXml());
            Console.ResetColor();
            Console.WriteLine("Response xml: ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(mobile.GetResponseXml());
            Console.ResetColor();
        }

        private static void Status(Config config)
        {
            var client = new OrderClientMerchant(config);
            var mobile = client.GetMobilePayment();
            Console.Write("Enter the transaction id: ");
            var transactionId = Console.ReadLine();
            if (String.IsNullOrEmpty(transactionId))
            {
                transactionId = "";
            }
            try
            {
                var response = mobile.Status(UInt64.Parse(transactionId));
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Status: Success");
                Console.ResetColor();
            }
            catch (ApiException e)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Status: Fail. Error code: {0}, message: {1}", e.ErrorCode, e.Message);
                Console.ResetColor();
            }
            Console.WriteLine("Request xml: ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(mobile.GetRequestXml());
            Console.ResetColor();
            Console.WriteLine("Response xml: ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine(mobile.GetResponseXml());
            Console.ResetColor();
        }

	    public static void Notify(Config config)
	    {
            var request = new Request
            {
                Notify = new Notify
                {
                    Status = "true",
                    Transaction = 32969,
                    ExtTransactionId = "548823a25cda4",
                    Amount = (decimal)12.0000,
                    Destination = new Destination
                    {
                        Account = "listepo",
                        Name = "wefwef"
                    },
                    PaymentSystemIncome = new PaymentSystemIncome
                    {
                        Sum = (decimal)12.0000,
                        Currency = "USD"
                    },
                    ServiceIncome = new ServiceIncome
                    {
                        Sum = (decimal)17.3913,
                        Currency = "USD"
                    }
                },
                Date = DateTime.Now.ToString("yyyy-MM-dd H:mm:ss")
            };

            Console.WriteLine("Request xml: ");
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.BackgroundColor = ConsoleColor.White;
            var data = Parser.Serealize(request);

            var requestData = Signature.SignData(data, File.ReadAllText(@"C:\N1_private.pem"));
            Console.WriteLine(requestData);
            Console.ResetColor();
	    }
	}
}
