﻿using System.IO;

namespace N1
{
    public class Config
    {
        public string ProcessingUrl = "https://processingsandbox-dev.n1.ua";
        public string MerchantName = "Sandbox_merchant";
        public string ServiceName = "Sandbox_service";
        public string PublicKey;
        public string PrivateKey;
        public int RequestTimeout = 12000;

        public void SetPublicKeyFromFile(string path)
        {
            PublicKey = File.ReadAllText(path);
        }

        public void SetPrivateKeyFromFile(string path)
        {
            PrivateKey = File.ReadAllText(path);
        }


        public string GetRequestUrl()
        {
            return ProcessingUrl + "/" + MerchantName;
        }
    }
}
