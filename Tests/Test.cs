using System.Globalization;
using NUnit.Framework;
using System;
using N1;
using N1.Xml;

namespace Tests
{
	[TestFixture]
	public class Test
	{
	    private static Config GetConfig()
	    {
            return new Config
            {
                MerchantName = "34f34g34g",
                ServiceName = "wefwef",
                RequestTimeout = 12000,
                PrivateKey = TestKeys.PrivateKey,
                PublicKey = TestKeys.PublicKey
            };
	    }
        [Test]
        public void TestCreate()
        {
            var config = GetConfig();
            var sender = new SenderMock();
            sender.SetResponse(DataMock.OrderClientMerchantCreateResponse);
            var client = new OrderClientMerchant(config, sender, DateTime.Parse("2014-12-03 13:18:40"));
            var mobile = client.GetMobilePayment();
            var response = mobile.Create("b095feb978b94e228f2f00d1e82e8f9f", 10, "listepo", "wefwef", "380634981206");
            Assert.AreEqual(sender.GetRequest(), DataMock.OrederClientMerchantCreateRequest);
            Assert.AreEqual(response.GetType(), typeof(Response));
            Assert.AreEqual(response.TransactionId, 30572);
            Assert.AreEqual(response.Date, "2014-12-03 12:06:02");
            Assert.AreEqual(response.Status, "true");
            Assert.AreEqual(response.StatusTransaction, null);
            Assert.AreEqual(response.Error, null);
            Assert.AreEqual(response.PaymentSystemIncome, null);
            Assert.AreEqual(response.ServiceIncome, null);
        }

        [Test]
        public void TestConfirm()
        {
            var config = GetConfig();
            var sender = new SenderMock();
            sender.SetResponse(DataMock.OrderClientMerchantConfirmResponse);
            var client = new OrderClientMerchant(config, sender, DateTime.Parse("2014-12-08 17:02:00"));
            var mobile = client.GetMobilePayment();
            var response = mobile.Confirm(32693, "30525");
            Assert.AreEqual(sender.GetRequest(), DataMock.OrederClientMerchantConfirmRequest);
            Assert.AreEqual(response.GetType(), typeof(Response));
            Assert.AreEqual(response.TransactionId, 0);
            Assert.AreEqual(response.Date, "2014-12-08 17:01:00");
            Assert.AreEqual(response.Status, "true");
            Assert.AreEqual(response.StatusTransaction, null);
            Assert.AreEqual(response.Error, null);
            Assert.AreEqual(response.Amount, 10.0000);
            Assert.AreEqual(response.PaymentSystemIncome.GetType(), typeof(PaymentSystemIncome));
            Assert.AreEqual(response.ServiceIncome.GetType(), typeof(ServiceIncome));
            Assert.AreEqual(response.PaymentSystemIncome.Sum, 14.4928);
            Assert.AreEqual(response.PaymentSystemIncome.Currency, "USD");
            Assert.AreEqual(response.ServiceIncome.Sum, 10.0000);
            Assert.AreEqual(response.ServiceIncome.Currency, "USD");
        }

        [Test]
        public void TestStatus()
        {
            var config = GetConfig();
            var sender = new SenderMock();
            sender.SetResponse(DataMock.OrderClientMerchantStatusResponse);
            var client = new OrderClientMerchant(config, sender, DateTime.Parse("2014-12-08 17:25:20"));
            var mobile = client.GetMobilePayment();
            var response = mobile.Status(32693);
            Assert.AreEqual(sender.GetRequest(), DataMock.OrederClientMerchantStatusRequest);
            Assert.AreEqual(response.GetType(), typeof(Response));
            Assert.AreEqual(response.TransactionId, 0);
            Assert.AreEqual(response.Date, "2014-12-08 17:23:48");
            Assert.AreEqual(response.Status, "true");
            Assert.AreEqual(response.StatusTransaction.GetType(), typeof(StatusTransaction));
            Assert.AreEqual(response.StatusTransaction.InternalId, 32693);
            Assert.AreEqual(response.StatusTransaction.Created, "2014-12-08 16:59:35");
            Assert.AreEqual(response.StatusTransaction.Step, "confirm");
            Assert.AreEqual(response.StatusTransaction.Status, "successful");
            Assert.AreEqual(response.Error, null);
            Assert.AreEqual(response.PaymentSystemIncome, null);
            Assert.AreEqual(response.ServiceIncome, null);
        }

        [Test]
	    public void TestNotify()
	    {
            var config = GetConfig();
            var client = new OrderClientMerchant(config);
            var test = new TestProcessor();
            var response = client.GetRequestProcessor(test).ProcessRequest(DataMock.OrederClientMerchantNotifyRequest);
            var notify = test.GetNotify();
            Assert.AreEqual(notify.GetType(), typeof(Notify));
            Assert.AreEqual(notify.Amount, 12.0000);
            Assert.AreEqual(notify.PaymentSystemIncome.GetType(), typeof(PaymentSystemIncome));
            Assert.AreEqual(notify.ServiceIncome.GetType(), typeof(ServiceIncome));
            Assert.AreEqual(notify.PaymentSystemIncome.Sum, 12m);
            Assert.AreEqual(notify.PaymentSystemIncome.Currency, "USD");
            Assert.AreEqual(notify.ServiceIncome.Sum, 17.3913m);
            Assert.AreEqual(notify.ServiceIncome.Currency, "USD");
            Assert.AreEqual(notify.Destination.Account, "listepo");
            Assert.AreEqual(notify.Destination.Name, "wefwef");
            Assert.AreEqual(notify.ExtTransactionId, "548823a25cda4");

	    }
	}
}

