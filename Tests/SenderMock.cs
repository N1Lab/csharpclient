﻿using N1.Interfaces;
using N1.Utilities;

namespace Tests
{
    class SenderMock : ISender
    {
        private string _response;
        private string _request;
        public string SendApiRequest(string data, string url, int timeout = 12000)
        {
            SetRequest(data);
            return _response;
        }

        public void SetResponse(string response)
        {
            _response = response;
        }

        public void SetRequest(string request)
        {
            _request = request;
        }

        public string GetRequest()
        {
            return _request;
        }        
    }
}
