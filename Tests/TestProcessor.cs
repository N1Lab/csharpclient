﻿using N1.Interfaces;
using N1.Xml;

namespace Tests
{
    class TestProcessor : IOrderPaymentProcessor
    {
        private Notify _notify;
        public Response Check(Check check)
        {
            throw new System.NotImplementedException();
        }

        public Response Confirm(Notify notify)
        {
            _notify = notify;
            return new Response
            {
                Status = "true"
            };
        }

        public Notify GetNotify()
        {
            return _notify;
        }
    }
}
