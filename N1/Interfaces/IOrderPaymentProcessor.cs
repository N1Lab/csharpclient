﻿using N1.Xml;

namespace N1.Interfaces
{
    public interface IOrderPaymentProcessor
    {
        Response Check(Check check);

        Response Confirm(Notify notify);
    }
}
