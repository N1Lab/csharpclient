﻿using N1.Xml;

namespace N1.Interfaces
{
    public interface IAccountPaymentProcessor
    {
        Response Check(Check check);
        Response Pay(Pay pay);
    }
}
