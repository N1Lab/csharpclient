﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace N1.Interfaces
{
    public interface ISender
    {
        string SendApiRequest(string data, string url, int timeout = 12000);
    }
}
