﻿using System;

namespace N1.Exceptions
{
    public class ApiException : Exception
    {
        public readonly int ErrorCode;
        public ApiException(int errorCode, string message)
            : base(message)
        {
            ErrorCode = errorCode;
        }
    }
}
