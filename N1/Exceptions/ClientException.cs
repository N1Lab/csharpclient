﻿using System;

namespace N1.Exceptions
{
    public class ClientException : Exception
    {
        public readonly ClientErrorCodes ErrorCode;
        public ClientException(ClientErrorCodes errorCode, string message)
            : base(message)
        {
            ErrorCode = errorCode;
        }
    }
}
