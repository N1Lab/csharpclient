﻿namespace N1.Exceptions
{
    public enum ClientErrorCodes
    {
        InvalidConfig,
        ResponseError,
        SignatureFaild,
        BadRequestType
    }
}
