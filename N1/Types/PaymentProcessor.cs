﻿using System;
using N1.Exceptions;
using N1.Interfaces;
using N1.Utilities;
using N1.Xml;

namespace N1.Types
{
    public abstract class PaymentProcessor
    {
        private const string SuccessResult = "true";
        protected readonly Config Config;
        public ISender Sender;
        public DateTime RequestDate;
        protected String RequestXml;
        protected String ResponseXml;
        protected PaymentProcessor(Config config, ISender sender)
        {
            CheckConfig(config);
            Config = config;
            Sender = sender;
            RequestDate = DateTime.Now;
        }
        protected PaymentProcessor(Config config, ISender sender, DateTime requestDate)
        {
            CheckConfig(config);
            Config = config;
            Sender = sender;
            RequestDate = requestDate;
        }

        protected Response SendApiRequest(Request request)
        {
            var data = Parser.Serealize(request);

            var requestData = Signature.SignData(data, Config.PrivateKey);
            RequestXml = requestData;
            var reaponseData = Sender.SendApiRequest(requestData, Config.GetRequestUrl(), Config.RequestTimeout);
            ResponseXml = reaponseData;
            var parser = (Response)Parser.Deserealize(typeof(Response), reaponseData, Config.PublicKey);

            if (parser.Status != SuccessResult) throw new ApiException(parser.Error.Code, parser.Error.Message);

            return parser;
        }

        protected void CheckConfig(Config config)
        {
            if (String.IsNullOrEmpty(config.ServiceName))
            {
                throw new ClientException(ClientErrorCodes.InvalidConfig, "Config.ServiceName is empty");
            }
            if (String.IsNullOrEmpty(config.MerchantName))
            {
                throw new ClientException(ClientErrorCodes.InvalidConfig, "Config.MerchantName is empty");
            }
            if (String.IsNullOrEmpty(config.PrivateKey))
            {
                throw new ClientException(ClientErrorCodes.InvalidConfig, "Config.PrivateKey is empty");
            }
            if (String.IsNullOrEmpty(config.PublicKey))
            {
                throw new ClientException(ClientErrorCodes.InvalidConfig, "Config.PublicKey is empty");
            }
        }

        public String GetRequestXml()
        {
            return RequestXml;
        }

        public String GetResponseXml()
        {
            return ResponseXml;
        }
    }
}
