﻿using System;
using System.Globalization;
using N1.Interfaces;
using N1.Utilities;
using N1.Xml;

namespace N1.Types
{
    public class MobilePayment : PaymentProcessor
    {
        public MobilePayment(Config config, ISender sender)
            : base(config, sender)
        {
        }

        public MobilePayment(Config config, ISender sender, DateTime requestDate)
            : base(config, sender, requestDate)
        {
        }

        public Response Create(string merchantTransactionId, decimal amount, string account, string service, string phoneNumber)
        {
            var request = new Request
            {
                Create = new Create
                {
                    Amount = amount,
                    ExtTransactionId = merchantTransactionId,
                    Destination = new Destination
                    {
                        Account = account,
                        Name = service
                    },
					Source = new Source { Name = "ksmobilecommerce" },
                    Parameters = new[] { new Parameter { Name = "PhoneNumber", Value = phoneNumber } }
                },
                Date = RequestDate.ToString("yyyy-MM-dd H:mm:ss")
            };

            return SendApiRequest(request);
        }

        public Response Confirm(UInt64 transactionId, string confirmCode)
        {
            var request = new Request
            {
                Confirm = new Confirm
                {
                    TransactionId = transactionId.ToString(CultureInfo.InvariantCulture),
                    Parameters = new[] { new Parameter { Name = "ConfirmCode", Value = confirmCode } }
                },
                Date = RequestDate.ToString("yyyy-MM-dd H:mm:ss")
            };

            return SendApiRequest(request);
        }


        public Response Status(UInt64 transactioonId)
        {
            var request = new Request
            {
                Status = new Status
                {
                    TransactionId = transactioonId.ToString(CultureInfo.InvariantCulture)
                },
                Date = RequestDate.ToString("yyyy-MM-dd H:mm:ss")
            };

            return SendApiRequest(request);
        }

    }
}
