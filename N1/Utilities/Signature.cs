﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;

namespace N1.Utilities
{
    public static class Signature
    {
        public static string SignData(string data, string privateKey)
        {
            data = data.Replace("<sign />", "<sign></sign>");
            var rsa = new RSACryptoServiceProvider { PersistKeyInCsp = false }; 
            rsa.LoadPrivateKeyPEM(privateKey);
            var signature = rsa.SignData(Encoding.UTF8.GetBytes(data), new SHA1CryptoServiceProvider());
            var result = Signature.BytesToHex(signature);
            return data.Replace("<sign></sign>", "<sign>" + result + "</sign>");
        }

        public static bool VerifyData(string data, string publicKey)
        {
            const string signPattern = @"<sign>(.*)</sign>";
            var sign = Regex.Match(data, signPattern).ToString().Replace("<sign>", "").Replace("</sign>", "");
            var rgx = new Regex(signPattern);
            var signData = rgx.Replace(data, "<sign></sign>");
            var rsa = new RSACryptoServiceProvider { PersistKeyInCsp = false };
            rsa.LoadPublicKeyPEM(publicKey);
            return rsa.VerifyData(Encoding.UTF8.GetBytes(signData), new SHA1CryptoServiceProvider(), StringToByteArray(sign.ToLower()));
        }

        private static string BytesToHex(byte[] ba)
        {
            var hex = BitConverter.ToString(ba);
            return hex.Replace("-", "");
        }
   
        private static byte[] StringToByteArray(String hex)
        {
            var numberChars = hex.Length;
            var bytes = new byte[numberChars / 2];
            for (var i = 0; i < numberChars; i += 2)
                bytes[i / 2] = Convert.ToByte(hex.Substring(i, 2), 16);
            return bytes;
        }
    }
}
