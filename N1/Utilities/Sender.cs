﻿using System.IO;
using System.Net;
using System.Text;
using N1.Exceptions;
using N1.Interfaces;

namespace N1.Utilities
{
    public class Sender : ISender
    {
        public string SendApiRequest(string data, string url, int timeout = 12000)
        {
            var byteArray = Encoding.UTF8.GetBytes(data);
            var request = WebRequest.Create(url);
            request.Method = "POST";
            request.Timeout = timeout;
            request.ContentType = "text/xml";
            request.ContentLength = byteArray.Length;
        
            var dataStream = request.GetRequestStream();
        
            dataStream.Write(byteArray, 0, byteArray.Length);
            
            dataStream.Close();

            var response = request.GetResponse();
            var stream = response.GetResponseStream();
            if (stream == null)
            {
                throw new ClientException(ClientErrorCodes.ResponseError, "No data stream");
            }
            var readStream = new StreamReader(stream, Encoding.UTF8);
            var text = readStream.ReadToEnd();
            return text;
        }
    }
}
