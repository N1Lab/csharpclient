﻿using System.IO;
using System.Text;

namespace N1.Utilities
{
    public class Utf8StringWriter : StringWriter
    {
        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }
}
