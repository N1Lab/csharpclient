﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using N1.Exceptions;

namespace N1.Utilities
{
    public static class Parser
    {
        public static string Serealize(Object data)
        {
            var formatter = new XmlSerializer(data.GetType());
            var stringWriter = new Utf8StringWriter();
            var ns = new XmlSerializerNamespaces();
            ns.Add(String.Empty, String.Empty);
            formatter.Serialize(stringWriter, data, ns);
            return stringWriter.ToString();
        }

        public static Object Deserealize(Type type, string reaponseData, string publicKey)
        {
            var status = Signature.VerifyData(reaponseData, publicKey);

            if (!status) throw new ClientException(ClientErrorCodes.SignatureFaild, "The signature is not valid");

            var formatter = new XmlSerializer(type);
            var xmlReader = XmlReader.Create(new StringReader(reaponseData));
            return formatter.Deserialize(xmlReader);
        }
    }
}
