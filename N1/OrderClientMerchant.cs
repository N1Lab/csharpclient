﻿using System;
using N1.Interfaces;
using N1.Types;
using N1.Utilities;

namespace N1
{
    public class OrderClientMerchant
    {
        private readonly Config _config;
        public ISender Sender;
        public DateTime RequestDate = DateTime.Now;
        public OrderClientMerchant(Config config)
        {
            _config = config;
            Sender = new Sender();
        }
        public OrderClientMerchant(Config config, ISender sender)
        {
            _config = config;
            Sender = sender;
        }

        public OrderClientMerchant(Config config, ISender sender, DateTime requestDate)
        {
            _config = config;
            Sender = sender;
            RequestDate = requestDate;
        }

        public MobilePayment GetMobilePayment()
        {
            return new MobilePayment(_config, Sender, RequestDate);
        }

        public RequestProcessor GetRequestProcessor(IOrderPaymentProcessor processor)
        {
            return new RequestProcessor(processor, _config);
        }
    }
}
