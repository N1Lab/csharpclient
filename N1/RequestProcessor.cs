﻿using System;
using N1.Exceptions;
using N1.Interfaces;
using N1.Utilities;
using N1.Xml;

namespace N1
{
    public class RequestProcessor
    {
        public readonly Config Config;
        public readonly IOrderPaymentProcessor OrderPaymentProcessor;
        public readonly IAccountPaymentProcessor AccountPaymentProcessor;
        public RequestProcessor(IOrderPaymentProcessor processor, Config config)
        {
            Config = config;
            OrderPaymentProcessor = processor;
        }

        public RequestProcessor(IAccountPaymentProcessor processor, Config config)
        {
            Config = config;
            AccountPaymentProcessor = processor;
        }

        public String ProcessRequest(String requestData)
        {
            var request = (Request)Parser.Deserealize(typeof(Request), requestData, Config.PublicKey);
            var response = new Response();
            if (OrderPaymentProcessor != null)
            {
               response = RunOrderPaymentProcessor(request);
            }
            if (AccountPaymentProcessor != null)
            {
                response = RunAccountPaymentProcessor(request);
            }
            var data = Parser.Serealize(response);
            return Signature.SignData(data, Config.PrivateKey);           
        }

        public Response RunOrderPaymentProcessor(Request request)
        {
            if (request.Check != null)
            {
                return OrderPaymentProcessor.Check(request.Check);
            }
            if (request.Notify != null)
            {
                return OrderPaymentProcessor.Confirm(request.Notify);
            }
            throw new ClientException(ClientErrorCodes.BadRequestType, "Request type not found");
        }

        public Response RunAccountPaymentProcessor(Request request)
        {
            if (request.Check != null)
            {
                return OrderPaymentProcessor.Check(request.Check);
            }
            if (request.Pay != null)
            {
                return AccountPaymentProcessor.Pay(request.Pay);
            }
            throw new ClientException(ClientErrorCodes.BadRequestType, "Request type not found");
        }
    }
}
