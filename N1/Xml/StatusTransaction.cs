﻿using System.Xml.Serialization;

namespace N1.Xml
{
    [XmlRoot("status_transaction")]
    public class StatusTransaction
    {
        [XmlElement("internal_id")]
        public ulong InternalId;

        [XmlElement("created")]
        public string Created;

        [XmlElement("step")]
        public string Step;

        [XmlElement("status")]
        public string Status;
    }
}
