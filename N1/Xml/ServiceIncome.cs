﻿using System;
using System.Xml.Serialization;

namespace N1.Xml
{
    [XmlRoot("service_income")]
    public class ServiceIncome
    {
        [XmlElement("sum")]
        public Decimal Sum;

        [XmlElement("currency")]
        public String Currency;
    }
}
