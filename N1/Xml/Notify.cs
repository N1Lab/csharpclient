﻿using System;
using System.Xml.Serialization;

namespace N1.Xml
{
    [XmlRoot("notify")]
    public class Notify
    {
        [XmlElement("status")]
        public String Status;

        [XmlElement("amount")]
        public Decimal Amount;

        [XmlElement("transaction")]
        public UInt64 Transaction;

        [XmlElement("ext_transaction_id")]
        public String ExtTransactionId;

        [XmlElement("destination")]
        public Destination Destination;

        [XmlElement("paymentsystem_income")]
        public PaymentSystemIncome PaymentSystemIncome;

        [XmlElement("service_income")]
        public ServiceIncome ServiceIncome;
    }
}
