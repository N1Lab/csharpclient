﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace N1.Xml
{
    [XmlRoot("status")]
    public class Status
    {
        [XmlElement("transaction_id")]
        public string TransactionId;

        [XmlElement("extTransaction")]
        public string ExtTransactionId;
    }
}
