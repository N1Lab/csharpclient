﻿using System;
using System.Xml.Serialization;

namespace N1.Xml
{
    [XmlRoot("paymentsystem_income")]
    public class PaymentSystemIncome
    {
        [XmlElement("sum")]
        public Decimal Sum;

        [XmlElement("currency")]
        public String Currency;
    }
}
