﻿using System.Xml.Serialization;
using N1.Interfaces;

namespace N1.Xml
{
    [XmlRoot("request")]
    public class Request
    {
        [XmlElement("date")]
        public string Date;

        [XmlElement("check")]
        public Check Check;

        [XmlElement("create")]
        public Create Create;

        [XmlElement("pay")]
        public Pay Pay;

        [XmlElement("notify")]
        public Notify Notify;

        [XmlElement("confirm")]
        public Confirm Confirm;

        [XmlElement("status")]
        public Status Status;

        [XmlElement("sign")]
        public string Sign= "";

        public Request(Create create)
        {
            Create = create;
        }

        public Request(){}
    }
}
