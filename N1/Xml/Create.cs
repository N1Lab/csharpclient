﻿using System.Xml.Serialization;

namespace N1.Xml
{
    [XmlRoot("create")]
    public class Create
    {
        [XmlElement("amount")]
        public decimal Amount;

        [XmlElement("ext_transaction_id")]
        public string ExtTransactionId;

        [XmlElement("destination")]
        public Destination Destination;

        [XmlElement("source")]
        public Source Source;

        [XmlArray(ElementName = "parameters")]
        [XmlArrayItem("parameter")]
        public Parameter []Parameters;

    }
}
