﻿using System.Xml.Serialization;

namespace N1.Xml
{
    [XmlRoot("error")]
    public class Error
    {
        [XmlElement("code")]
        public int Code;

        [XmlElement("message")]
        public string Message;
    }
}
