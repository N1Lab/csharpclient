﻿using System.Xml.Serialization;

namespace N1.Xml
{
    [XmlRoot("destination")]
    public class Destination
    {
        [XmlElement("account")]
        public string Account;

        [XmlElement("name")]
        public string Name;
    }
}
