﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace N1.Xml
{
    [XmlRoot("source")]
    public class Source
    {
        [XmlElement("name")]
        public string Name;
    }
}
