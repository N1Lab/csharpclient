﻿using System;
using System.Xml.Serialization;
using N1.Interfaces;

namespace N1.Xml
{
    [XmlRoot("response")]
    public class Response
    {
        [XmlElement("status")]
        public String Status;

        [XmlElement("date")] 
        public String Date;

        [XmlElement("transaction_id")]
        public UInt64 TransactionId;

        [XmlElement("error")]
        public Error Error;

        [XmlElement("status_transaction")]
        public StatusTransaction StatusTransaction;

        [XmlElement("amount")]
        public Decimal Amount;

        [XmlElement("paymentsystem_income")]
        public PaymentSystemIncome PaymentSystemIncome;

        [XmlElement("service_income")]
        public ServiceIncome ServiceIncome;

        public DateTime GetDate()
        {
            return DateTime.Parse(Date);
        }
    }
}
