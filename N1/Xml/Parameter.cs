﻿using System.Xml.Serialization;

namespace N1.Xml
{
    [XmlRoot(ElementName = "parameter")]
    public class Parameter
    {
        [XmlAttribute("name")]
        public string Name;

        [XmlText]
        public string Value;
    }
}
