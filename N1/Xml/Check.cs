﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace N1.Xml
{
    [XmlRoot("check")]
    public class Check
    {
        [XmlElement("destination")]
        public Destination Destination;
    }
}
