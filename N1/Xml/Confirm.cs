﻿using System.Xml.Serialization;

namespace N1.Xml
{
    [XmlRoot("confirm")]
    public class Confirm
    {
        [XmlElement("transaction_id")]
        public string TransactionId;

        [XmlElement("extTransaction")]
        public string ExtTransactionId;

        [XmlArray(ElementName = "parameters")]
        [XmlArrayItem("parameter")]
        public Parameter[] Parameters;
    }
}
